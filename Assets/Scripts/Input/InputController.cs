﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {

    public delegate void OnKeyboardDirection(Vector2 direction);
    public delegate void OnKeyboardShoot();

    public event OnKeyboardDirection OnKeyboardDirectionEvent;
    public event OnKeyboardShoot OnKeyboardShootEvent;

    public static InputController Instance
    {
        get;
        private set;
    }

	void Awake()
	{
        Instance = this;
	}
	
	void Update () 
	{
        if (OnKeyboardDirectionEvent != null)
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                OnKeyboardDirectionEvent(Vector2.up);
            }
	    
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                OnKeyboardDirectionEvent(-Vector2.right);
            }
            else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                OnKeyboardDirectionEvent(Vector2.right);
            }
        }

        if (OnKeyboardShootEvent != null)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                OnKeyboardShootEvent();
            }
        }
	}
}
