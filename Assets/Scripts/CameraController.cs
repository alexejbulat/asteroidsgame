﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour {

    public Bounds CameraBounds
    {
        get;
        private set;
    }

    public static CameraController Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        Instance = this;

        UpdateCameraBounds();
    }

    private void UpdateCameraBounds()
    {
        float distance = (camera.transform.position - Vector3.zero).magnitude;
        float frustumHeight = 2.0f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        float frustumWidth = frustumHeight * camera.aspect;
        CameraBounds = new Bounds(Vector3.zero, new Vector3(frustumWidth, frustumHeight, 0));
    }
}
