﻿using System.Collections;
using UnityEngine;

public class Spaceship : MonoBehaviour
{

    public int MaxLifes;
    public float MaxHealth;

    public int CurrentLifes;
    public float CurrentHealth;

    public int Score;

    private BaseWeapon[] _weapons;

    public delegate void OnDestroyed(Spaceship spaceship);
    public event OnDestroyed OnDestroyedEvent;

    // Use this for initialization
    void Start()
    {
        InputController.Instance.OnKeyboardDirectionEvent += OnKeyboardDirection;
        InputController.Instance.OnKeyboardShootEvent += OnShootEvent;

        _weapons = GetComponentsInChildren<BaseWeapon>(true);

        CurrentLifes = MaxLifes;
        CurrentHealth = MaxHealth;
    }

    private void OnShootEvent ()
    {
        foreach(BaseWeapon weapon in _weapons)
        {
            weapon.Shoot(new Vector2(transform.up.x, transform.up.y));
        }
    }

    private void OnKeyboardDirection (Vector2 direction)
    {
        if (direction == Vector2.up)
        {
            Accelerate();
        }
        else if (direction == Vector2.right || direction == -Vector2.right)
        {
            Turn(direction);
        }
    }

    void OnCollisionEnter2D(Collision2D collision) 
    {
        if (collision.relativeVelocity.magnitude < 1f)
        {
            return;
        }
        foreach(ContactPoint2D contact in collision.contacts)
        {
            DamagableObject dmgObject = contact.collider.gameObject.GetComponent<DamagableObject>();
            if (dmgObject != null)
                OnDamageTaken(contact.point, dmgObject.RequestDamage());
        }
    }

    void OnDamageTaken(Vector3 position, float dmg)
    {
        CurrentHealth -= dmg;
        if (CurrentHealth <= 0)
        {
            OnDeath();
        }
    }

    void OnDeath()
    {
        CurrentLifes--;
        CurrentHealth = MaxHealth;
        if (CurrentLifes <= 0 )
        {
            if (OnDestroyedEvent != null)
            {
                OnDestroyedEvent(this);
            }
        }
        else
        {
            StartCoroutine(DamageShow());
        }

    }

    void OnDestroy()
    {
        InputController.Instance.OnKeyboardDirectionEvent -= OnKeyboardDirection;
        InputController.Instance.OnKeyboardShootEvent -= OnShootEvent;
    }

    void Accelerate()
    {
        Vector2 currentDirection = new Vector2(transform.up.x, transform.up.y);
        float magnitude = rigidbody2D.velocity.magnitude + Time.deltaTime * 2;
        rigidbody2D.velocity = Mathf.Min(magnitude, 2) * currentDirection;
    }

    void Turn(Vector2 direction)
    {
        float angle = -direction.x * 90f * Time.deltaTime;
        float angularForce = rigidbody2D.angularVelocity - direction.x * Time.deltaTime * 130f;
        rigidbody2D.angularVelocity = Mathf.Min(angularForce, 90f);
    }

    private IEnumerator DamageShow()
    {
        this.collider2D.enabled = false;
        SpriteRenderer sr = this.GetComponent<SpriteRenderer>();
        sr.color = new Color(1f, 1f, 1f, 0.1f);
        yield return new WaitForSeconds(2f);
        sr.color = new Color(1f, 1f, 1f, 1f);
        this.collider2D.enabled = true;
    }
}
