﻿using UnityEngine;
using System.Collections;

public interface IScreen {

    void OnDraw();
}
