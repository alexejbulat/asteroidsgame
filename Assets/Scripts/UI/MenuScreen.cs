﻿using UnityEngine;
using System;
using System.Collections;

public class MenuScreen : IScreen {

    public Action OnPlayAction;
    public int BestScore;

    public void OnDraw()
    {
        float btnWidth = Screen.width/4f;
        float btnHeight = btnWidth * 0.2f;
        if (GUI.Button(new Rect(Screen.width/2f - btnWidth/2f, Screen.height/2f, btnWidth, btnHeight), "Play"))
        {
            OnPlayAction();
        }

        GUI.Label(new Rect(10, 10, Screen.width/2f, 40), "Best Score: " + BestScore);
    }
}
