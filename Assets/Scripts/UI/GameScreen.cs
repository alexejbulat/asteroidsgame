﻿using UnityEngine;
using System.Collections;

public class GameScreen : IScreen {

    public Spaceship Spaceship;
    public int CurrentScore;

	public void OnDraw()
    {
        if (Spaceship != null)
        {
            GUI.Label(new Rect(10, 10, Screen.width/2f, 30), "Lifes: " + Spaceship.CurrentLifes + "/" + Spaceship.MaxLifes);
            GUI.Label(new Rect(10, 30, Screen.width/2f, 30), "Health: " + (int)Spaceship.CurrentHealth + "/" + (int)Spaceship.MaxHealth);
            GUI.Label(new Rect(10, 50, Screen.width/2f, 30), "Score: " + CurrentScore);
        }
    }
}
