﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

    public GameScreen GameScreen = new GameScreen();
    public MenuScreen MenuScreen = new MenuScreen();

    private IScreen _currentScreen;

    void OnGUI()
    {
        if (_currentScreen != null)
        {
            _currentScreen.OnDraw();
        }
    }

    public void ChangeScreen(IScreen screen)
    {
        _currentScreen = screen;
    }
}
