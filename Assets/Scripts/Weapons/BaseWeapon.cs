﻿using UnityEngine;
using System.Collections;

public abstract class BaseWeapon : MonoBehaviour {

    public GameObject AmmoPrefab;

    public abstract void Shoot(Vector2 direction);
}
