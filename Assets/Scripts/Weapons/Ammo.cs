﻿using UnityEngine;
using System.Collections;

public class Ammo : DamagableObject{

    private static GameObject _ammoRoot;
	
	void Update () {

        float angle = Mathf.Atan2(rigidbody2D.velocity.y, rigidbody2D.velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle - 90f, Vector3.forward);
	}

    void OnCollisionEnter2D(Collision2D collision) 
    {
        Destroy(this.gameObject);
    }

    void OnBecameInvisible() 
    {
        GameObject.Destroy(this.gameObject);
    }

    public static GameObject InstantiateAmmo(GameObject ammoPrefab)
    {
        if (_ammoRoot == null)
        {
            _ammoRoot = new GameObject("root_ammo");
        }
        GameObject ammoGO = GameObject.Instantiate(ammoPrefab) as GameObject;
        ammoGO.transform.parent = _ammoRoot.transform;
        return ammoGO;
    }
}
