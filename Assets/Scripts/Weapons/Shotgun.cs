﻿using UnityEngine;
using System.Collections;

public class Shotgun : BaseWeapon {

    private const float FireRate = 10f;

    private float _latestShootTime;

    public override void Shoot(Vector2 direction)
    {
        if (AmmoPrefab == null)
        {
            return;
        }

        if (Time.time - _latestShootTime < 1f/FireRate)
        {
            return;
        }

        GameObject ammoGO = Ammo.InstantiateAmmo(AmmoPrefab);
        ammoGO.transform.position = this.transform.position;
        ammoGO.rigidbody2D.AddForce(direction * 500f);
        _latestShootTime = Time.time;
    }


}
