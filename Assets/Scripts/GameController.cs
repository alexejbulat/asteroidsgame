﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIController))]
public class GameController : MonoBehaviour {

    public Spaceship Spaceship;
    public AsteroidsController asteroidsController;

    public int CurrentScore;

    private UIController uiController;

    void Awake()
    {
        uiController = GetComponent<UIController>();
    }

	void Start () 
    {
        uiController.GameScreen.Spaceship = Spaceship;
        Spaceship.OnDestroyedEvent += OnSpaceshipDestroyed;
        Spaceship.gameObject.SetActive(false);
        uiController.ChangeScreen(uiController.MenuScreen);
        uiController.MenuScreen.BestScore = PlayerPrefs.GetInt("best_score");

        asteroidsController.OnScoreAction = (score) =>
        {
            CurrentScore += score;
            uiController.GameScreen.CurrentScore = CurrentScore;
        };

        uiController.MenuScreen.OnPlayAction = () =>
        {
            asteroidsController.Reset();
            Spaceship.gameObject.SetActive(true);
            uiController.ChangeScreen(uiController.GameScreen);
        };
	}

    void OnSpaceshipDestroyed (Spaceship spaceship)
    {
        spaceship.gameObject.SetActive(false);
        uiController.ChangeScreen(uiController.MenuScreen);
        int bestScore = PlayerPrefs.GetInt("best_score");
        if (CurrentScore > bestScore)
        {
            bestScore = CurrentScore;
            PlayerPrefs.SetInt("best_score", bestScore);
            PlayerPrefs.Save();
        }
        uiController.MenuScreen.BestScore = bestScore;
        uiController.GameScreen.CurrentScore = 0;
        CurrentScore = 0;
    }
	
}
