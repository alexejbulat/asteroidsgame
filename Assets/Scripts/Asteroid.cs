﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Asteroid : DamagableObject{

    public float Strength;
    public int Score;
    public AsteroidType type;
    public GameObject ExplotionParticlePrefab;

    public Bounds OutterBounds;

    public delegate void OnDestroyed(Asteroid asteroid);
    public event OnDestroyed OnDestroyedEvent;

    void Update()
    {
        if (OutterBounds.Contains(this.transform.position) == false)
        {
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision) 
    {
        foreach(ContactPoint2D contact in collision.contacts)
        {
            DamagableObject dmgObject = contact.collider.gameObject.GetComponent<DamagableObject>();
            if (dmgObject != null)
                OnHit(contact.point, dmgObject.RequestDamage());
        }
    }

    private void OnHit(Vector3 hitPosition, float dmg)
    {
        Strength -= dmg;
        ExplotionParticle.InstantiateParticle(ExplotionParticlePrefab, hitPosition);
        if (Strength <= 0)
        {

            if (OnDestroyedEvent != null)
            {
                OnDestroyedEvent(this);
            }
            Destroy(this.gameObject);
        }
    }
}
