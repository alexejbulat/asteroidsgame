﻿using UnityEngine;
using System.Collections;

public class DamagableObject: MonoBehaviour
{

    public float DamageMin;
    public float DamageMax;

    public float RequestDamage()
    {
        return Random.Range(DamageMin, DamageMax);
    }
}
