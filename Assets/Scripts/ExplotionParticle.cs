﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ExplotionParticle : MonoBehaviour {

    private static GameObject _particlesRoot;

	void Start () 
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        StartCoroutine(DestroyParticle(ps.duration));
	}
	
    private IEnumerator DestroyParticle(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(this.gameObject);
    }

    public static ParticleSystem InstantiateParticle(GameObject prefab, Vector3 position)
    {
        if (_particlesRoot == null)
        {
            _particlesRoot = new GameObject("root_particles");
        }
        GameObject particleGO = GameObject.Instantiate(prefab) as GameObject;
        particleGO.transform.parent = _particlesRoot.transform;
        particleGO.transform.position = position;
        return particleGO.GetComponent<ParticleSystem>();
    }
}
