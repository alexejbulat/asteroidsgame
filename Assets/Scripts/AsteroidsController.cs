﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AsteroidType
{
    Huge,
    Large,
    Middle,
    Small
}

public class AsteroidsController : MonoBehaviour {

    public GameObject[] AsteroidPrefabs;
    public GameObject AsteroidsShootParticles;
    public System.Action<int> OnScoreAction;

    private float nextTimestamp;
    private Dictionary<AsteroidType, GameObject> _cachedPrefabs = new Dictionary<AsteroidType, GameObject>();
    private Bounds _cameraBounds;

    private static GameObject _asteroidsRoot;

	// Use this for initialization
	void Start () {
        _cameraBounds = CameraController.Instance.CameraBounds;
        InitPrefabs();
	}
	
    void Update()
    {
        if (Time.time >= nextTimestamp)
        {
            float delta = 2f;
            CreateAsteroid(AsteroidPrefabs[Random.Range(0, AsteroidPrefabs.Length)], GenerateRandomPoint(delta));
            nextTimestamp += Random.Range(1, 2);
        }
    }

    private void InitPrefabs()
    {
        foreach(GameObject prefab in AsteroidPrefabs)
        {
            Asteroid asteroid = prefab.GetComponent<Asteroid>();
            if (_cachedPrefabs.ContainsKey(asteroid.type) == false)
            {
                _cachedPrefabs.Add(asteroid.type, prefab);
            }
        }
    }

    public void Reset()
    {
        if (_asteroidsRoot != null)
        {
            Destroy(_asteroidsRoot);
        }
    }

    public Asteroid[] CreateAsteroids(int count, AsteroidType type, Vector3 position)
    {
        Asteroid[] result = new Asteroid[count];
        for (int i=0; i<count; i++)
        {
            result[i] = CreateAsteroid(type, position);
        }
        return result;
    }

    public Asteroid CreateAsteroid(AsteroidType type, Vector3 position)
    {
        return CreateAsteroid(_cachedPrefabs[type], position);
    }

    public Asteroid CreateAsteroid(GameObject prefab, Vector3 position)
    {
        if (_asteroidsRoot == null)
        {
            _asteroidsRoot = new GameObject("root_asteroids");
        }
        GameObject asteroidGO = GameObject.Instantiate(prefab) as GameObject;
        asteroidGO.transform.parent = _asteroidsRoot.transform;
        asteroidGO.transform.position = position;
        Asteroid asteroid = asteroidGO.GetComponent<Asteroid>();
        asteroid.ExplotionParticlePrefab = AsteroidsShootParticles;
        Bounds outterBounds = _cameraBounds;
        outterBounds.Expand(20f);
        asteroid.OutterBounds = outterBounds;
         
        Vector3 rndVelocity = (GenerateRandomPoint(0) - position).normalized * Random.Range(10, 100) * asteroid.rigidbody2D.mass;
        asteroid.rigidbody2D.AddForce(new Vector2(rndVelocity.x, rndVelocity.y));
        asteroid.OnDestroyedEvent += OnAsteroidDestroyed;
        return asteroid;
    }

    private void OnAsteroidDestroyed (Asteroid asteroid)
    {
        OnScoreAction(asteroid.Score);
        List<Asteroid> childs = new List<Asteroid>();
        RecursiveCreation(asteroid.type, asteroid.transform.position, childs);
        foreach(Asteroid child in childs)
        {
            child.transform.position = asteroid.transform.position + new Vector3(Random.value, Random.value) * child.renderer.bounds.size.magnitude;
            child.rigidbody2D.AddForce((child.transform.position - asteroid.transform.position).normalized * Random.Range(1, 10) * child.rigidbody2D.mass);
        }
    } 

    private void RecursiveCreation(AsteroidType startType, Vector3 position, List<Asteroid> created, int count = 0)
    {
        if (startType == AsteroidType.Huge)
        {
            RecursiveCreation(AsteroidType.Large, position, created, Random.Range(0, 2));
            return;
        }
        else if (startType == AsteroidType.Large)
        {
            RecursiveCreation(AsteroidType.Middle, position, created, Random.Range(1, 2));
        }
        else if (startType == AsteroidType.Middle)
        {
            RecursiveCreation(AsteroidType.Small, position, created, Random.Range(1, 3));
        }

        created.AddRange( CreateAsteroids(count, startType, position));
    }

    private Vector3 GenerateRandomPoint(float delta)
    {
        Vector3 position = Vector3.zero;
        if (Random.Range(0, 2) == 0)
        {
            position.x = Random.Range(_cameraBounds.min.x - delta, _cameraBounds.max.x + delta);
            position.y = Random.Range(0, 2) == 0 ? 
                Random.Range(_cameraBounds.min.y - delta, _cameraBounds.min.y) : 
                    Random.Range(_cameraBounds.max.y, _cameraBounds.max.y + delta); 
            position.z = 0;
        }
        else
        {
            position.x = Random.Range(0, 2) == 0 ? 
                Random.Range(_cameraBounds.min.x - delta, _cameraBounds.min.x) : 
                    Random.Range(_cameraBounds.max.x, _cameraBounds.max.x + delta); 
            position.y = Random.Range(_cameraBounds.min.y - delta, _cameraBounds.max.y + delta);
            position.z = 0;
        }
        return position;
    }
}
